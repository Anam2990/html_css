<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-CC0100EN-SkillsNetwork/labs/IBMCloud_accountCreation/images/IDSNlogo.png" width="300">

# Laboratorio práctico: Crear una cuenta de IBM Cloud

**Tiempo estimado:** 15 minutes

## Resumen del laboratorio:

Para acceder a los recursos y servicios que ofrece IBM Cloud necesita una cuenta IBM Cloud. Este laboratorio le ayudará paso a paso a crear una cuenta.

## Objectivos:

Después de terminar este laboratorio usted será capaz de:

*   Crear una cuenta IBM Cloud de prueba utilizando un código especial:

## Requisitos:

Antes de comenzar este laboratorio asegúrese por favor de que cuenta con un código especial obtenido en secciones anteriores del curso y que nunca haya sido utilizado.

> **Nota:** Estas instrucciones son unicamente para crear una cuenta **NUEVA IBM Cloud** utilizando una dirección de correo que no haya utilizado previamente para crear otra cuenta IBM Cloud. Si ya tiene una cuenta de prueba y quiere utilizar un Código Especial para extender su periodo de prueba siga por favor las instrucciones en [lab](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-CC0100EN-SkillsNetwork/labs/IBMCloud_accountCreation/ApplyFeatureCode.md.html) utilizando el código de su cuenta IBM Cloud.

Si ha creado anteriormente una cuenta IBM y el periodo de prueba a terminado necesitará crear entonces una cuenta nueva con una dirección de correo diferente.

# Crear una Cuenta IBM Cloud

1.  Vaya a: [https://cloud.ibm.com/registration](https://cloud.ibm.com/registration?utm_medium=Exinfluencer&utm_source=Exinfluencer&utm_content=000026UJ&utm_term=10006555&utm_id=NA-SkillsNetwork-Channel-SkillsNetworkCoursesIBMCC0100ENSkillsNetwork30551557-2021-01-01) para crear una cuenta IBM Cloud.

2.  Ingrese su dirección de **correo electrónico** y una **contraseña** segura como se solicita y haga clic en el botón **Siguiente (Next)**.

     <img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0201ES-Coursera/labs/images/IBM_CloudAcc_1.png" width="75%" style="border: solid 1px">

> **NOTA:** Por favor asegúrese de que utiliza una dirección de correo no utilizada anteriormente para crear una cuenta IBM y a la que tiene acceso para poder recibir el código de verificación requerido en el siguiente paso.

3.  En correo electrónico es enviado a la dirección que eligió para confirmarla. Revise su correo y copie y pegue el **Código de Verificación**. Después haga clic en **Siguiente(Next)**.

     <img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0201ES-Coursera/labs/images/IBM_CloudAcc_2.png" width="75%" style="border: solid 1px">

4.  Una vez verificado su correo electrónico ingrese su **Nombre**, **Apellido** y **País**. Después haga clic en **Continuar(Next)**

     <img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0201ES-Coursera/labs/images/IBM_CloudAcc_3.png" width="75%" style="border: solid 1px">

5.  Vaya a los Avisos de Cuenta y active las actualizaciones vía correo si lo desea, acepte las condiciones y haga clic en **Continuar**,

     <img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0201ES-Coursera/labs/images/IBM_CloudAcc_4.png" width="75%" style="border: solid 1px">

6.  Antes de crear su cuenta revise las políticas de privacidad y acepte que las ha leído y entendido marcando la casilla, después haga clic en Continuar.

     <img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0201ES-Coursera/labs/images/IBM_CloudAcc_5.png" width="75%" style="border: solid 1px">

> Toma unos segundos crear y configurar su cuenta.

7.  Seleccione **Personal** como su tipo de cuenta y haga clic en \***Registrar mediante un código**

     <img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0201ES-Coursera/labs/images/IBM_CloudAcc_6.png" width="75%" style="border: solid 1px">

> **Nota:** Por favor asegúrese de escoger la opción de Código Especial (Feature Code) para poder completar este y otros cursos. No debe utilizar la opción de Tarjeta de Crédito para verificar su cuenta ya que podrían cargarse cobros innecesarios en el proceso de activación.

8.  Copie el código de la sección **Obtener código para IBM Cloud (Claim IBM Cloud Feature Code)** de su curso y pegue donde indica **Ingresar Código**. Haga clic en **Crear Cuenta**.

<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0201ES-Coursera/labs/images/IBM_CloudAcc_7.png" width="75%" style="border: solid 1px">

> **Nota:** Puede tomar algunos minutos crear su cuenta.

9.  Una vez que haya creado con éxito su cuenta de IBM Cloud deberá poder ver el panel de control.

<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-CC0100EN-SkillsNetwork/labs/IBMCloud_accountCreation/images/Step_10.png" width="75%" style="border: solid 1px">

1.  Ahora puede explorar el [Catálogo IBM](https://cloud.ibm.com/catalog?utm_medium=Exinfluencer&utm_source=Exinfluencer&utm_content=000026UJ&utm_term=10006555&utm_id=NA-SkillsNetwork-Channel-SkillsNetworkCoursesIBMCC0100ENSkillsNetwork30551557-2021-01-01) para ver los servicios y recursos que ofrece IBM.

<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-CC0100EN-SkillsNetwork/labs/IBMCloud_accountCreation/images/catalog.png" style="border: solid 1px grey">

¡Felicidades! ¡Ha creado su cuenta IBM Cloud!

## Autor(es)

<h4> Lavanya <h4/>

### Otros colaboradores

Upkar Lidder

## Changelog

| Fecha       | Versión | Modificado por | Descripción                                                                |
| ----------- | ------- | -------------- | -------------------------------------------------------------------------- |
| 2021-01-28  | 1.0     | Lavanya        | Añadir nuevas instrucciones para crear una cuenta IBM Cloud                |
| 2022-03-15  | 2.0     | Samaah         | Actualizar nuevas instrucciones e imagenes para crear una cuenta IBM Cloud |

## <h3 align="center"> Â© IBM Corporation 2022. All rights reserved. <h3/>