# Applying a feature code to your IBM Cloud account
The special feature code provided above will allow you to extend your IBM Cloud Trial by 6-months. This is intended to enable you complete hands-on labs and projects in courses from IBM and its partners and help enhance your learning experience with hands-on skills.

## Pre-requisite
In order to apply the the feature code, you should have already created an IBM Cloud Trial account by following the [instructions in this lab](CreateIBMCloudAccount.md.html) . 

NOTE: This code cannot be applied to Pay-As-You-Go or Subscription accounts, or if your trial has already expired.

## Instructions
Kindly follow the instructions below to apply the code:

1. Copy the feature code that is generated. You can click to the copy button to the right of the code to do so easily.
    <img src="images/fc1.png"/>


2. In the IBM Cloud console, go to Manage > Account, and select [Account settings](https://cloud.ibm.com/account/settings).

    <img src="images/fc2.png"/>


3. Click **Apply code** button under the `Subscription and feature codes section`.

    <img src="images/fc3.png"/>


4. Paste the alphanumeric feature code you copied in Step 1 above and click on **Apply**:

    <img src="images/fc4.png"/>

5. Your trial should now be extended by 6-months. 

    <img src="images/fc5.png"/>


**If you encounter an error when applying your code, please consult these knowledge base articles:**
-  [Server Error](http://support.cognitiveclass.ai/knowledgebase/articles/1829818) 
-  [Unable to apply Feature Code](http://support.cognitiveclass.ai/knowledgebase/articles/1829872) 
-  [Feature Code has already been used](http://support.cognitiveclass.ai/knowledgebase/articles/1829878) 
-  [Upgrade to reactivate your account](http://support.cognitiveclass.ai/knowledgebase/articles/1830685)

