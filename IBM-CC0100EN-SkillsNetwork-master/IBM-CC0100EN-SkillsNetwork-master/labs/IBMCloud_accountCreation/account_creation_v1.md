<img src="images/IDSNlogo.png" width = "300">

# Hands on Lab: Creating IBM Cloud account

**Estimated Effort:** 10 minutes

## Lab overview:
To access the resources and services that the IBM Cloud provides, you need an IBM Cloud account. This lab will take you through step by step instructions to create an IBM Cloud account if you do not already have one or your access has expired.

## Objectives:
After completing this lab, you will be able to:
* Create an account on IBM Cloud

# Create an IBM Cloud Trial Account

>NOTE: Some hands-on labs/assignments in this course require the use of services on IBM Cloud. In this lab you will be creating an IBM Cloud **Trial** account after which you will be able to access select IBM Cloud services and plans for 30-days at no charge, without the need to enter a credit card. In case you are unable to complete the course in 30-days, and your trial has expired, you may need to create another trial account with a different email address, or apply a Feature Code to extend the trial, if one has been provided to you by your course provider / institution.  

Please follow the instructions below to create an account on IBM Cloud:

1.  Go to: [https://cloud.ibm.com/registration/trial](https://cloud.ibm.com/registration/trial) to create a trial account on IBM Cloud.

2. Enter your company **Email** address and a strong **Password**, as per criteria and then click the **Next** button.
<img src="images/1-trial-email.png"/>
> NOTE: Please ensure that you provide an email address that you have not used previously to create any other IBM Cloud account, and you are able to readily access your email to retrieve the verification code required in the next step.

3. An email is sent to the address that you signed up with to confirm your email address. Check your email and copy and paste **Verification code**. Then click **Next**.
<img src="images/2-trial-email.png"/>

4. Once your email is successfully verified, enter your **First name**, **Last name**, and **Country** and click **Next**.
<img src="./images/3-trial-email.png"/>

5. Go through the **Account Notice** and opt for Email updates if you desire, accept the terms and conditions and click on **Continue**.
<img src="./images/4-trial-email.png"/>

6. Before creating your account, review the account privacy notice and acknowledge that you have read and understood by checking the checkbox and click on **Continue**.
<img src="./images/review_privacy.png"/>

> **It takes a few seconds to create and set up your account.**

7. You will be taken to the login screen. The username (which is your email address) is already populated. 
<img src="./images/login_screen.png"/>

8. Enter the password that you had chosen for the login. Unless you are using a private computer, don't choose to save the password.
<img src="./images/password_screen.png"/>

9. Once you successfully login, you should see the dashboard. 
<img src="./images/dashboard.png" style="border: solid 1px grey"/>

10. Explore the [IBM Catalog](https://cloud.ibm.com/catalog) for exploring the services and resources offered by IBM Cloud.
<img src="./images/catalog.png" style="border: solid 1px grey"/>


## Author(s)
<h4> Lavanya <h4/>

### Other Contributor(s) 
Anamika

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2021-01-26 | 1.3 | Rav | Updated Trial account info |
| 2021-11-23 | 1.2 | Srishti | Updated screenshots of Trial link |
| 2021-11-22 | 1.1 | Rav | Updated instructions to have only Trial link |
| 2021-10-18 | 1.0 | Lavanya | Created new instructions for IBM Cloud Account creation with Trial option |



## <h3 align="center"> © IBM Corporation 2021. All rights reserved. <h3/>

