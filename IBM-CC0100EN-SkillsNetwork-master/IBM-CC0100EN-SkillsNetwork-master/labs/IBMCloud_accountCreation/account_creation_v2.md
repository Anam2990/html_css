<img src="images/IDSNlogo.png" width = "300">

# Hands on Lab: Creating IBM Cloud Account

**Estimated Effort:** 15 minutes

## Lab overview:
To access the resources and services that the IBM Cloud provides, you need an IBM Cloud account. This lab will take you through step by step instructions to create IBM Cloud account.

## Objectives:
After completing this lab, you will be able to:
* Create an IBM Cloud Trial Account using a Feature Code    

## Prerequisite:
Prior to starting this lab please ensure you have obtained an IBM Cloud Feature Code from a previous section of the course and have not applied that code previously.

> **Note:** These instructions only apply to creating a **NEW IBM Cloud** account using an email address that you have not used previously to create another IBM Cloud account. If you already have an IBM Cloud Trial account, and want to apply a Feature Code to extend your trial period, kindly follow the instructions in the lab: [Applying Feature Code to your IBM Cloud Account](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-CC0100EN-SkillsNetwork/labs/IBMCloud_accountCreation/ApplyFeatureCode.md.html) .   
If you previously created an IBM Cloud account and the trial has expired, you will need to create a new account with a different email address.

# Create an IBM Cloud Account

1.  Go to: [https://cloud.ibm.com/registration](https://cloud.ibm.com/registration) to create a account on IBM Cloud.

2. Enter your **Email** address and a strong **Password**, as per criteria and then click the **Next** button.

    <img src="./images/Step_2.png" width="75%" style="border: solid 1px"/>

> **NOTE:** Please ensure that you provide an email address that you have not used previously to create any other IBM Cloud account, and you are able to readily access your email to retrieve the verification code required in the next step.

3. An email is sent to the address that you signed up with to confirm your email address. Check your email and copy and paste **Verification code**. Then click **Next**.

    <img src="./images/Step_3.png" width="75%" style="border: solid 1px"/>

4. Once your email is successfully verified, enter your **First name**, **Last name**, and select **Country** then click **Next**.

    <img src="./images/Step_4.png" width="75%" style="border: solid 1px"/>


5. Go through the Account Notice and opt for Email updates if you desire, accept the terms and conditions and click on **Continue**.

    <img src="./images/Step_5.png" width="75%" style="border: solid 1px"/>

6. Before creating your account, review the account privacy notice and acknowledge that you have read and understood by checking the checkbox and click on Continue

    <img src="./images/Step_6.png" width="75%" style="border: solid 1px"/>

> It takes a few seconds to create and set up your account.

7. Select ***Personal*** as your account type and click on ***Register with a code**

    <img src="./images/Step_8.png" width="75%" style="border: solid 1px"/>

> **Note:** Please ensure you choose the **Register with a code** option for the purposes of completing this and other courses. You should not use the Credit Card option to verify your account for course work as that can result in unnecessary charges and delays in activating your account.

8. Copy the Feature Code from ***Obtain IBM Cloud Feature Code*** section of your course and paste it under the **Enter Code**. Then click **Create account**. 


<img src="./images/Step_9.png" width="75%" style="border: solid 1px"/>

> **Note:** It might take a couple of minutes to create your account.

9. Once you've successfully created your IBM Cloud account, you should see the dashboard.

<img src="./images/Step_10.png" width="75%" style="border: solid 1px"/>

10. Now you may explore the [IBM Catalog](https://cloud.ibm.com/catalog) for exploring the services and resources offered by IBM Cloud.
<img src="./images/catalog.png" style="border: solid 1px grey"/>

Congratulations! You have successfully created your IBM Cloud Account!

## Author(s)
<h4> Lavanya <h4/>

### Other Contributor(s) 
Rav Ahuja

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2021-01-28 | 1.0 | Lavanya | Created new instructions for IBM Cloud Account creation |
| 2022-03-15 | 2.0 | Samaah | Updated new instructions and images for IBM Cloud Account creation |
| 2022-03-22 | 2.1 | Rav | Clarifications and edits |

## <h3 align="center"> © IBM Corporation 2022. All rights reserved. <h3/>
