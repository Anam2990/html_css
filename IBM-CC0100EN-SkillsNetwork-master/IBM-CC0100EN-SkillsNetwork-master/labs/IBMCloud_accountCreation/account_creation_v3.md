---
markdown-version: v1
tool-type: instructional-lab
branch: lab-2797-instruction
version-history-start-date: '2022-12-15T09:29:39Z'
---
<img src="/images/IDSN-logo.png" width = "300">

# Hands on Lab: Creating IBM Cloud Account

**Estimated Effort:** 15 minutes

## Lab overview:
To access the resources and services that the IBM Cloud provides, you need an IBM Cloud account. This lab will take you through step by step instructions to create IBM Cloud account.

## Objectives:
After completing this lab, you will be able to:
* Create an IBM Cloud Trial Account using a Feature Code

## Prerequisite:
Please ensure you click on the **"Open Tool"** button from the previous section of this lab to get a feature code and activate the trial account before moving forward with the lab instructions. These instrucctions will help you create an IBM Cloud account.

> **Note:** These instructions only apply to creating a **NEW IBM Cloud** account using an email address that you have not used previously to create another IBM Cloud account. If you have already used your IBM Cloud feature code in another course/lab to create an IBM Cloud account, please skip this item, as the code can only be used once.

# Activate Trial Account

1. The previous section of the lab is the first step for activating your IBM Cloud trial account using Feature Code.

2. On clicking on `Open Tool` button, you will get a unique code.

3. Next, click on Activate Your Trial. It will redirect you to the IBM Cloud registration page, to create an account on IBM Cloud.

![](./images/activate_trial_account.png)

# Create an IBM Cloud Account

1.  Once you are on the [account creation](https://cloud.ibm.com/registration) page, follow the below instructions to create an IBM cloud trial account.

2. Enter your **Email** address and a strong **Password**, as per criteria and then click the **Next** button.

![](./images/Step_2.png)

> **NOTE:** Please ensure that you provide an email address that you have not used previously to create any other IBM Cloud account, and you are able to readily access your email to retrieve the verification code required in the next step.

3. An email is sent to the address that you signed up with to confirm your email address. Check your email and copy and paste **Verification code**. Then click **Next**.

![](./images/Step_3.png)

4. Once your email is successfully verified, enter your **First name**, **Last name**, and preferably select **United States** under **Country or region** then click **Next**.

![](./images/Step_4.png)

5. Go through the Account Notice and opt for Email updates if you desire, accept the terms and conditions and click on **Continue**.

![](./images/Step_5.png)

6. Before creating your account, review the account privacy notice and acknowledge that you have read and understood by checking the checkbox and click on Continue

![](./images/Step_6.png)

> It takes a few seconds to create and set up your account.

7. On the next screen, you will be asked to verify identity where you will see the feature code has been applied for you already. Then click on **Create account**. 

![](./images/verify_identity.png)

> **Note:** It might take a couple of minutes to create your account.

> **Note:** Please ensure you should not use the Credit Card option to verify your account for course work as that can result in unnecessary charges and delays in activating your account.

8. Once you've successfully created your IBM Cloud account, you should see the dashboard.

![](./images/Step_10.png)

9. Now you may explore the [IBM Catalog](https://cloud.ibm.com/catalog) for exploring the services and resources offered by IBM Cloud.
<img src="./images/catalog.png" style="border: solid 1px grey"/>

# Ways To Troubleshoot

If you encounter any of the below errors while activating your IBM Cloud trial account, please follow the steps as instructed below.

- Ooops snap! The promotional codes for IBM Cloud are very popular and have temporarily run out. Please check back soon.

    **Reason** - It take 24 to 48 hours to populate news feature codes.

    **Solution** - Please try again later

- Something went wrong error.

    **Reason** - This could be because you have already applied a code to this email earlier or your domain/country/ip is restricted.

    **Solution** - Try from another email id

- Feature code expired.

    **Reason** - You may have applied the feature code already once.

    **Solution** - Create a new IBM cloud account and write to the support team as per instructions below.

- If your trial ends.

    **Reason** - Trial accounts are valid only for 6 months, and the same feature code cannot be applied again.

    **Solution** - Create a new IBM cloud account and write to the support team as per instructions below.

## Other Possible Solutions:

- Try clearing your browser's cache and cookies
- Try from a different browser on an incognito mode.

If you still face any issues, please write an email to [support@cognitiveclass.ai](support@cognitiveclass.ai) with the following details

**Subject Line:** Feature Code Issue

**Email Content:**

- Name of the course you are undertaking.
- The course link where you obtained the feature code
- The feature code you are trying to apply.
- The error message that it is showing.
- The email ID that is being used to create the IBM Cloud account.
- Your username.

Once you obtain a new feature code, you can create a new account using a different Email ID.

Congratulations! You have successfully created your IBM Cloud Account!

## Author(s)
<h4> Lavanya <h4/>

### Other Contributor(s)
Rav Ahuja

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2021-01-28 | 1.0 | Lavanya | Created new instructions for IBM Cloud Account creation |
| 2022-03-15 | 2.0 | Samaah | Updated new instructions and images for IBM Cloud Account creation |
| 2022-03-22 | 2.1 | Rav | Clarifications and edits |
| 2022-05-16 | 2.2 | Srishti | Updated new instructions and images for IBM Cloud Account creation |

## <h3 align="center"> © IBM Corporation 2022. All rights reserved. <h3/>
