<img src="images/IDSNlogo.png" width = "300">

# Hands on Lab: Creating IBM Cloud account

**Estimated Effort:** 10 minutes

## Lab overview:
To access the resources and services that the IBM Cloud provides, you need an IBM Cloud account. This lab will take you through step by step instructions to create IBM Cloud account.

## Objectives:
After completing this lab, you will be able to:
* Create an account on IBM Cloud

# Create an IBM Cloud Trial Account

Please follow the instructions below to create an account on IBM Cloud:

1.  Go to: [https://cloud.ibm.com/registration/trial](https://cloud.ibm.com/registration/trial) to create a trial account on IBM Cloud.

2. Enter your company **Email** address and a strong **Password**, as per criteria and then click the **Next** button.
<img src="images/1-trial-email.png"/>

3. An email is sent to the address that you signed up with to confirm your email address. Check your email and copy and paste **Verification code**. Then click **Next**.
<img src="images/2-trial-email.png"/>

4. Once your email is successfully verified, enter your **First name**, **Last name**, and **Country** and click **Next**.
<img src="./images/3-trial-email.png"/>

5. Go through the **Account Notice** and opt for Email updates if you desire, accept the terms and conditions and click on **Continue**.
<img src="./images/4-trial-email.png"/>

6. Before creating your account, review the account privacy notice and acknowledge that you have read and understood by checking the checkbox and click on **Continue**.
<img src="./images/review_privacy.png"/>

> **It takes a few seconds to create and set up your account.**

7. You will be taken to the login screen. The username (which is your email address) is already populated. 
<img src="./images/login_screen.png"/>

8. Enter the password that you had chosen for the login. Unless you are using a private computer, don't choose to save the password.
<img src="./images/password_screen.png"/>

9. Once you successfully login, you should see the dashboard. 
<img src="./images/dashboard.png" style="border: solid 1px grey"/>

10. Explore the [IBM Catalog](https://cloud.ibm.com/catalog) for exploring the services and resources offered by IBM Cloud.
<img src="./images/catalog.png" style="border: solid 1px grey"/>


## Author(s)
<h4> Lavanya <h4/>

### Other Contributor(s) 
Anamika

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2022-01-25 | 1.0 | Srishti | Created lab for Cognitive Class Courses |



## <h3 align="center"> © IBM Corporation 2021. All rights reserved. <h3/>
