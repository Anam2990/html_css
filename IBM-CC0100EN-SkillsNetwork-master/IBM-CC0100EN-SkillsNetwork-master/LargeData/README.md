# LargeData

This folder is a placeholder for publishing large file or object that cannot be managed by Git. We will bypass GitLab and directly upload content to Asset Library (Object Storage).